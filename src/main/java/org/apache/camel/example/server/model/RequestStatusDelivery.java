
package org.apache.camel.example.server.model;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RequestStatusDelivery {

    @XmlElement(required = true)
    protected String msgIdExt;

    public String getMsgIdExt() {
        return msgIdExt;
    }

    public void setMsgIdExt(String value) {
        this.msgIdExt = value;
    }

}
