
package org.apache.camel.example.server.model;

import javax.xml.bind.annotation.*;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseStatusDelivery {

    @XmlElement(required = true)
    protected String msgIdExt;
    @XmlElement(required = true)
    protected String statusDelivery;

    public String getMsgIdExt() {
        return msgIdExt;
    }

    public void setMsgIdExt(String value) {
        this.msgIdExt = value;
    }

    public String getStatusDelivery() {
        return statusDelivery;
    }

    public void setStatusDelivery(String value) {
        this.statusDelivery = value;
    }

}
