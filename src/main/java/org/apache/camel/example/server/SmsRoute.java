package org.apache.camel.example.server;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.converter.jaxb.JaxbDataFormat;
import org.apache.camel.example.server.model.RequestStatusDelivery;
import org.apache.camel.example.server.model.ResponseStatusDelivery;

public class SmsRoute extends RouteBuilder {


    @Override
    public void configure() throws Exception {
        JaxbDataFormat jaxb = new JaxbDataFormat(RequestStatusDelivery.class.getPackage().getName());

        from("spring-ws:rootqname:{http://types.smsgatesupport.sbrf.com.ua}requestStatusDelivery?endpointMapping=#endpointMapping")
                .unmarshal(jaxb)
                .process(new SmsProcessor())
                .marshal(jaxb);
    }

    private static final class SmsProcessor implements Processor {
        public void process(Exchange exchange) throws Exception {
            RequestStatusDelivery requestStatusDelivery = exchange.getIn().getBody(RequestStatusDelivery.class);
            ResponseStatusDelivery responseStatusDelivery = new ResponseStatusDelivery();
            String res = requestStatusDelivery.getMsgIdExt();
            responseStatusDelivery.setMsgIdExt(res);
            System.out.println(res);
            exchange.getOut().setBody(responseStatusDelivery);
        }
    }
}
